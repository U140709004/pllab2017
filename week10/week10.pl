#!/usr/bin/perl

use strict;
use warnings;

my $file_name = $ARGV[0];

open FILE, '<', $file_name or die "Cannot open $file_name!\n";

my @lines = <FILE>;

close FILE;

foreach my $l (@lines){
	chomp $l;
	if($l =~ /(.*) \- \- \[(.*) \-(.*)\] \"(.*) (.*) HTTP\/(.*)\" (\d{3}) (.*)/){
		print "$1 $7	";
		if($7 < 300){print "Success\n";}
		elsif($7 < 400){print "Redirect\n";}
		elsif($7 < 500){print "Client error\n";}
		else{print "Server error\n";}
		
	}
}

