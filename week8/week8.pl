use strict;
use warnings;
# File: readname.pl
# Read in name and age

print "Enter your name: ";
my $name = <>;
# "<>" reads one line from "standard input"
chomp $name;
# chomp deletes any newlines from end of string
print "Enter your age: ";
my $age = <>;
chomp $age;

print "Hello, $name!  ";
print "On your next birthday, you will be ", $age+1, ".\n";


my $p = 10 / 7;
printf "I have written %f programs.\n", $p;
printf "I have written %6.2f programs.\n", $p;
printf "I have written %0.2f programs.\n", $p;

exit;

my @a=("ali");

if (@a) {print "The lenght of \@a=".scalar@a. "\n";}
else {print "\@a is undefined or empty\n";}

my $str = "Hello World!";

my $x = substr $str, 0, 8;
print "\$x is: $x\n";

substr $str,7 ,5, "$name";
print "$str\n";





exit;